<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\{
    Controller, NotFoundHttpException
};
use services\CoinServiceFactory;
use common\models\{
    ParserForm, Coin, search\CoinSearch, Series
};

/**
 * CoinController implements the CRUD actions for Coin model.
 */
class CoinController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Parse action
     * @return string|\yii\web\Response
     */
    public function actionParse()
    {
        $model = new ParserForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($service = CoinServiceFactory::build($model->url)) {
                $service->create();
                return $this->redirect('index');
            }
        }
        return $this->render('parser', [
            'model' => $model,
        ]);
    }

    /**
     * Lists all Coin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'series' => Series::getAll(),
        ]);
    }

    /**
     * Displays a single Coin model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider(['query' => $model->getDetails()]);
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Coin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Coin();
        if ($model->load(Yii::$app->request->post())) {
            $model->uploadImage();
            $model->uploadImage('reverse');
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Запись успешно сохранена.');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', $model->getFirstErrors());
                return $this->refresh();
            }
        }

        return $this->render('create', [
            'model' => $model,
            'series' => Series::getAll(),
        ]);
    }

    /**
     * Updates an existing Coin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->uploadImage();
            $model->uploadImage('reverse');
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Запись успешно сохранена.');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', $model->getFirstErrors());
                return $this->refresh();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'series' => Series::getAll(),
        ]);
    }

    /**
     * Deletes an existing Coin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Coin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Coin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coin::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
