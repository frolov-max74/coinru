<?php

namespace backend\controllers;

use yii\web\Controller;
use common\models\chart\CoinsMap;

/**
 * Class ChartController
 * @package backend\controllers
 */
class ChartController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = (new CoinsMap())->getFull();

        return $this->render('index', [
            'data' => $data,
        ]);
    }

}
