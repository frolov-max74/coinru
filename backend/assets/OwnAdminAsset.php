<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Backend application asset bundle.
 */
class OwnAdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/admin.css',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
