<?php

use yii\helpers\Html;
use common\helpers\CoinHelper;
use yii\grid\GridView;
use yii\widgets\{
    DetailView,
    Pjax
};

/* @var $this yii\web\View */
/* @var $model common\models\Coin */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Coins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-primary',
        ]) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            [
                'attribute' => 'obverse',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $data->obverse,
                        [
                            'class' => 'view-coin',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'reverse',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $data->reverse,
                        [
                            'class' => 'view-coin',
                        ]
                    );
                }
            ],
            'description:html',
            'catalogue_number',
            [
                'attribute' => 'release_date',
                'format' => ['date', 'dd.MM.Y'],
            ],
            'thematic_info:html',
            'dignity',
            'material',
            [
                'attribute' => 'series_id',
                'value' => function ($data) {
                    return $data->series && $data->series->name != '-' ?
                        $data->series->name : 'нет';
                }
            ],
            [
                'attribute' => 'mint',
                'format' => 'raw',
                'value' => function ($data) {
                    return CoinHelper::mintLabel($data->mint);
                }
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    return CoinHelper::typeLabel($model->type);
                },
            ],
        ],
    ]) ?>

    <h3>Варианты исполнения</h3>
    <p>
        <?= Html::a(
            'Добавить вариант',
            [
                'details/create',
                'coin_id' => $model->id
            ],
            [
                'class' => 'btn btn-primary',
            ]
        ) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // 'id',
            // 'coin_id',
            'content:html',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'details',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>