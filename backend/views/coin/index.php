<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use common\helpers\CoinHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CoinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $series array all Series names */

$this->title = 'Coins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-index">

    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a(
            'Create Coin', ['create'],
            [
                'class' => 'btn btn-success',
            ]
        ) ?>
        <?= Html::a(
            'Reset', ['index'],
            [
                'class' => 'btn btn-default',
            ]
        ) ?>
    </p>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'obverse',
                    'label' => 'Аверс',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a(
                            Html::img(
                                Yii::$app->params['baseUrl'] . $data->obverse,
                                [
                                    'class' => 'grid-coin',
                                ]
                            ),
                            '/coin/view/?id=' . $data->id,
                            ['data-pjax' => 0]
                        );
                    }
                ],
                [
                    'attribute' => 'reverse',
                    'label' => 'Реверс',
                    'enableSorting' => false,
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a(
                            Html::img(
                                Yii::$app->params['baseUrl'] . $data->reverse,
                                [
                                    'class' => 'grid-coin',
                                ]
                            ),
                            '/coin/view/?id=' . $data->id,
                            ['data-pjax' => 0]
                        );
                    }
                ],
                'dignity',
                'name',
                'material',
                'catalogue_number',
                [
                    'attribute' => 'release_date',
                    'format' => ['date', 'php:d.m.Y'],
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'release_date',
                        'language' => 'ru',
                        'convertFormat' => true,
                        'startAttribute' => 'date_from',
                        'endAttribute' => 'date_to',
                        'pluginOptions' => [
                            'timePicker' => true,
                            'timePickerIncrement' => 30,
                            'locale' => [
                                'format' => 'd.m.Y'
                            ]
                        ]
                    ]),
                ],
                [
                    'attribute' => 'type',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'type',
                        CoinHelper::typeList(),
                        ['prompt' => 'Все']
                    ),
                    'value' => function ($data) {
                        return CoinHelper::typeLabel($data->type);
                    },
                ],
                [
                    'attribute' => 'mint',
                    'format' => 'raw',
                    'filter' => Html::activeCheckboxList(
                        $searchModel,
                        'mint',
                        CoinHelper::mintList(),
                        ['separator' => '<br>']
                    ),
                    'value' => function ($data) {
                        //var_dump($data->mint);die;
                        return CoinHelper::mintLabel($data->mint);
                    }
                ],
                [
                    'attribute' => 'series_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'series_id',
                        $series,
                        [
                            'prompt' => 'Все',
                        ]
                    ),
                    'value' => function ($data) {
                        return $data->series && $data->series->name != '-' ?
                            $data->series->name : 'нет';
                    }
                ],
                //'description:ntext',
                //'thematic_info:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>