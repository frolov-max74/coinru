<?php

/* @var $this yii\web\View */
/* @var $model common\models\Coin */
/* @var $series array array of the names of the series */

$this->title = 'Create Coin';
$this->params['breadcrumbs'][] = ['label' => 'Coins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-create">

    <?= $this->render('_form', [
        'model' => $model,
        'series' => $series,
    ]) ?>

</div>
