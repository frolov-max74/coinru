<?php

/* @var $this yii\web\View */
/* @var $model common\models\Coin */
/* @var $series array array of the names of the series */

$this->title = 'Update Coin: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Coins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coin-update">

    <?= $this->render('_form', [
        'model' => $model,
        'series' => $series,
    ]) ?>

</div>
