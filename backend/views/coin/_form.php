<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use yii\jui\DatePicker;
use common\helpers\CoinHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Coin */
/* @var $form yii\widgets\ActiveForm */
/* @var $series array array of the names of the series */
?>

<div class="coin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $model->obverse ? Html::img(
        Yii::$app->params['baseUrl'] . $model->obverse
    ) : ''; ?>

    <?= $form->field($model, 'obverseFile')->fileInput()->label('Аверс') ?>

    <?= $model->reverse ? Html::img(
        Yii::$app->params['baseUrl'] . $model->reverse
    ) : ''; ?>

    <?= $form->field($model, 'reverseFile')->fileInput()->label('Реверс') ?>

    <?= $form->field($model, 'description')->widget(CKEditor::class, [
        'editorOptions' => [
            'preset' => 'full',
            'inline' => false,
        ],
    ]); ?>

    <?= $form
        ->field($model, 'catalogue_number')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'release_date')->widget(DatePicker::class, [
        'language' => 'ru',
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

    <?= $form->field($model, 'thematic_info')->widget(CKEditor::class, [
        'editorOptions' => [
            'preset' => 'full',
            'inline' => false,
        ],
    ]); ?>

    <?= $form->field($model, 'dignity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'material')->textInput(['maxlength' => true]) ?>

    <?= $form
        ->field($model, 'mint')
        ->checkboxList(
            CoinHelper::mintList(),
            ['separator' => ' ']
        ) ?>

    <?= $form
        ->field($model, 'type')
        ->dropDownList(
            CoinHelper::typeList(),
            ['prompt' => 'Выбрать']
        ) ?>

    <?= $form->field($model, 'series_id')->dropDownList($series) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
