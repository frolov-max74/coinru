<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\CoinSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'obverse') ?>

    <?= $form->field($model, 'reverse') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'catalogue_number') ?>

    <?php // echo $form->field($model, 'release_date') ?>

    <?php // echo $form->field($model, 'thematic_info') ?>

    <?php // echo $form->field($model, 'dignity') ?>

    <?php // echo $form->field($model, 'material') ?>

    <?php // echo $form->field($model, 'series_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
