<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\ParserForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Parser';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-parser">

    <p>Please fill out the following fields to parsing:</p>

    <div class="row">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'parser-form']); ?>

            <?= $form->field($model, 'url')->textInput([
                'placeholder' => 'http://cbr.ru/Bank-notes_coins/coins_base/ShowCoins/?cat_num=....',
                'autofocus' => true,
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Parsing', ['class' => 'btn btn-primary', 'name' => 'parser-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
