<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Details */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'coin_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::class, [
        'editorOptions' => [
            'preset' => 'full',
            'inline' => false,
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
