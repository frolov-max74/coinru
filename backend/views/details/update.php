<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Details */

$this->title = 'Update Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="details-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
