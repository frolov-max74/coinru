<?php

use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $data array */

$this->title = 'Charts of Coins';
?>
    <div class="row">
        <div class="chart-index col-md-6">
            <?= Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Памятные и юбилейные монеты России'],
                    'subtitle' => ['text' => '(Все монеты по годам)'],
                    'chart' => [
                        'type' => 'spline',
                    ],

                    'xAxis' => [
                        'type' => 'date',
                        'categories' => $data['allDates'],
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество, шт.'],
                    ],
                    'series' => [
                        ['name' => 'Монеты России', 'data' => $data['allQuantities']],
                    ]
                ]
            ]); ?>
        </div>
        <div class="chart-index col-md-6">
            <?= Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Памятные и юбилейные монеты России'],
                    'subtitle' => ['text' => '(Некоторые серии монет)'],
                    'chart' => [
                        'type' => 'column',
                    ],

                    'xAxis' => [
                        'type' => 'date',
                        'categories' => $data['allDates'],
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество, шт.'],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'stacking' => 'normal',
                        ],
                    ],
                    'series' => [
                        ['name' => 'Российская Федерация', 'data' => $data['quantitiesByRFSeries']],
                        ['name' => 'Древние города России', 'data' => $data['quantitiesByAncientTownsSeries']],
                        ['name' => 'Без серии', 'data' => $data['quantitiesWithoutSeries']],
                        ['name' => 'Города воинской славы', 'data' => $data['quantitiesByMilitaryGloryCitiesSeries']],
                        ['name' => 'Выдающиеся личности России', 'data' => $data['quantitiesByProminentFiguresSeries']],
                        ['name' => 'Красная книга', 'data' => $data['quantitiesByRedBookSeries']],
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="chart-index col-md-6">
            <?= Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Памятные и юбилейные монеты России'],
                    'subtitle' => ['text' => '(Драгоценные и недрагоценные монеты)'],
                    'chart' => [
                        'type' => 'column',
                    ],

                    'xAxis' => [
                        'type' => 'date',
                        'categories' => $data['allDates'],
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество, шт.'],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'stacking' => 'normal',
                        ],
                    ],
                    'series' => [
                        ['name' => 'Драгоценные монеты', 'data' => $data['quantitiesByPrecious']],
                        ['name' => 'Недрагоценные монеты', 'data' => $data['quantitiesByNotPrecious']],
                    ]
                ]
            ]); ?>
        </div>
        <div class="chart-index col-md-6">
            <?= Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Памятные и юбилейные монеты России'],
                    'subtitle' => ['text' => '(Драгоценные монеты из разных металлов)'],
                    'chart' => [
                        'type' => 'column',
                    ],

                    'xAxis' => [
                        'type' => 'date',
                        'categories' => $data['allDates'],
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Количество, шт.'],
                    ],
                    'plotOptions' => [
                        'column' => [
                            'stacking' => 'normal',
                        ],
                    ],
                    'series' => [
                        ['name' => 'Золотые монеты', 'data' => $data['quantitiesByAu']],
                        ['name' => 'Серебряные монеты', 'data' => $data['quantitiesByAg']],
                        ['name' => 'Платиновые монеты', 'data' => $data['quantitiesByPt']],
                        ['name' => 'Палладиевые монеты', 'data' => $data['quantitiesByPd']],
                    ]
                ]
            ]); ?>
        </div>
    </div>
<?php
$this->registerJs('$(\'text.highcharts-credits\').remove()');
