<?php

use yii\db\Migration;

/**
 * Class m180813_164947_alter_name_column_in_coin_table
 */
class m180813_164947_alter_name_column_in_coin_table extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%coin}}', 'name', 'string(200) NOT NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%coin}}', 'name', 'string(150) NOT NULL');
    }
}
