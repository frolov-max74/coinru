<?php

use yii\db\Migration;

/**
 * Handles the creation of table `series`.
 */
class m180109_060305_create_series_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('series', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
        ]);

        $this->createIndex(
            'idx-series-name',
            'series',
            'name'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('series');
    }
}
