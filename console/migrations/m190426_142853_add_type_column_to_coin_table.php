<?php

use yii\db\Migration;

/**
 * Handles adding type to table `coin`.
 */
class m190426_142853_add_type_column_to_coin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%coin}}', 'type', 'smallint(1) NOT NULL default \'0\' AFTER `mint`');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%coin}}', 'type');
    }
}
