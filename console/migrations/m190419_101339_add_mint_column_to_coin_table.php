<?php

use yii\db\Migration;

/**
 * Handles adding mint to table `coin`.
 */
class m190419_101339_add_mint_column_to_coin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%coin}}', 'mint', 'string(100) NOT NULL AFTER `material`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%coin}}', 'mint');
    }
}
