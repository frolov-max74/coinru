<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coin`.
 */
class m180109_063438_create_coin_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('coin', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150),
            'obverse' => $this->string(150)->notNull(),
            'reverse' => $this->string(150)->notNull(),
            'description' => $this->text()->notNull(),
            'catalogue_number' => $this->string(20)->notNull()->unique(),
            'release_date' => $this->integer()->notNull(),
            'thematic_info' => $this->text(),
            'dignity' => $this->string(20)->notNull(),
            'material' => $this->string(100)->notNull(),
            'series_id' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-coin-series_id',
            'coin',
            'series_id'
        );
        $this->addForeignKey(
            'fk-coin-series',
            'coin',
            'series_id',
            'series',
            'id',
            'SET NULL',
            'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('coin');
    }
}
