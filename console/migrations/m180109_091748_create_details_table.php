<?php

use yii\db\Migration;

/**
 * Handles the creation of table `details`.
 */
class m180109_091748_create_details_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('details', [
            'id' => $this->primaryKey(),
            'coin_id' => $this->integer()->notNull(),
            'content' => $this->text(),
        ]);

        $this->createIndex(
            'idx-details-coin_id',
            'details',
            'coin_id'
        );
        $this->addForeignKey(
            'fk-details-coin',
            'details',
            'coin_id',
            'coin',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('details');
    }
}
