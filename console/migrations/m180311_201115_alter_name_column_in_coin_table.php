<?php

use yii\db\Migration;

/**
 * Class m180311_201115_alter_name_column_in_coin_table
 */
class m180311_201115_alter_name_column_in_coin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%coin}}', 'name', 'string(150) NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%coin}}', 'name', 'string(150)');
    }
}
