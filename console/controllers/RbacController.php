<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * Class RbacController
 * @package console\controllers
 */
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // remove old data
        $auth->removeAll();

        // add permission "viewDashboard"
        $dashboard = $auth->createPermission('viewDashboard');
        $dashboard->description = 'View dashboard';
        $auth->add($dashboard);

        //TODO: complete this method
    }
}
