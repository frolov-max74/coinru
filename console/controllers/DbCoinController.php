<?php

namespace console\controllers;

use common\models\db\Coin;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class DbCoinController
 * @package console\controllers
 */
class DbCoinController extends Controller
{
    /**
     * Command receives all catalogue numbers from the database,
     * if there is no number in the file, writes it to a file.
     */
    public function actionCatNumbersToFile()
    {
        $fileName     = dirname(__FILE__, 2) . '/data/catNums.txt';
        $resource     = fopen($fileName, 'a');
        $dataFromFile = file($fileName, FILE_SKIP_EMPTY_LINES);
        $catNums      = Coin::findAllCatalogueNumbers();

        foreach ($catNums as $catNum) {
            $isCatNum = in_array(
                $catNum['catalogue_number'] . PHP_EOL,
                $dataFromFile
            );

            if (!$isCatNum) {
                fwrite($resource, $catNum['catalogue_number'] . PHP_EOL);
                $num = $this->ansiFormat(
                    $catNum['catalogue_number'],
                    Console::FG_GREEN
                );
                echo 'Catalog number: ' . $num . ' was added to file' . "\n";
            }
        }

        fclose($resource);
        echo $this->ansiFormat('Done!', Console::FG_CYAN) . "\n";
    }
}
