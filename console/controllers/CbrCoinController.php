<?php

namespace console\controllers;

use common\models\db\Coin;
use services\CbrCoinService;
use yii\{
    base\InvalidArgumentException,
    console\Controller,
    helpers\Console
};
use DiDom\{
    Document,
    Element
};
use DateTime;
use DatePeriod;
use DateInterval;

/**
 * Class CbrCoinController
 * @package console\controllers
 */
class CbrCoinController extends Controller
{
    /** @var string URL base coins */
    private $urlBaseCoins =
        'http://cbr.ru/Bank-notes_coins/memorable_coins/coins_base/';

    /** @var string[] array of extracted catalog numbers */
    private $catNumbers = [];

    /** @var string[] array of catalog numbers from the database */
    private $catNumbersFromDb = [];

    /**
     * Command of parsing the coins that are not in the database,
     * depending on the parameter passed.
     *
     * @param null|string|int $year
     */
    public function actionParse($year = null)
    {
        if ('all' === $year) {
            $this->setCatNumbersFromFile();
        } else {
            $this->setCatNumbersFromDocument($year);
        }

        $this->setCatNumbersFromDb();
        $this->process(
            $this->getArrayDiffExtractedAndFromDb(),
            $this->urlBaseCoins
        );
    }

    /**
     * Sets the $catNumbers property from a file
     */
    private function setCatNumbersFromFile()
    {
        $fileName = dirname(__FILE__, 2) . '/data/catNums.txt';

        if (file_exists($fileName)) {
            /**
             * when reading a file,
             * skip the empty line and ignore the line feed
             */
            $this->catNumbers = file(
                $fileName,
                FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES
            );
        }
    }

    /**
     * Sets the $catNumbers property from a document
     *
     * @param null|string|int $year
     */
    private function setCatNumbersFromDocument($year)
    {
        switch ($year) {
            case null:
                $document = new Document($this->urlBaseCoins, true);

                break;
            case in_array($year, $this->getAllYearsOfRelease()):
                $queryString = $this->getQueryString([
                    'UniDbQuery.Posted' => 'true',
                    'UniDbQuery.Year'   => $year,
                ]);
                $document = new Document(
                    $this->urlBaseCoins . '?' . $queryString,
                    true
                );

                break;
            default:
                $year = $this->ansiFormat($year, Console::FG_RED);
                throw new InvalidArgumentException(
                    'Invalid Argument: ' . $year
                );
        }

        $spans = $document->find('div.gcolumns > span');

        /** @var Element $span */
        foreach ($spans as $span) {
            $this->catNumbers[] = $span->attr('data-id');
        }
    }

    /**
     * Returns all years from 1992 to the current
     */
    private function getAllYearsOfRelease(): array
    {
        $years  = [];
        $period = new DatePeriod(
            new DateTime('1992-01-01'),
            new DateInterval('P1Y'),
            new DateTime()
        );

        foreach ($period as $date) {
            /** @var DateTime $date */
            $years[] = $date->format('Y');
        }

        return $years;
    }

    /**
     * Returns a query string by params
     *
     * @param array $params
     *
     * Use like the following:
     *
     * ```php
     * $params = [
     *     'UniDbQuery.Posted'       => 'true', // if "false" then other parameters are useless
     *     'UniDbQuery.sortOrder'    => 0,      // if "1" then sorting by nominal
     *     'UniDbQuery.scViewType'   => 0,      // if "1" then the result is in the form of a table
     *     'UniDbQuery.SearchPhrase' => '',     // if it’s not empty then the search by name or by the catalog number
     *     'UniDbQuery.Year'         => 0,      // if "0" then for all years - not recomended!
     *     'UniDbQuery.Seria'        => 0,      // if "0" then everything, if "1" then without a name, otherwise there must be a unique identifier
     *     'UniDbQuery.Nominal'      => -1,     // if "-1" then everything, otherwise there must be a unique identifier
     *     'UniDbQuery.MetallID'     => 0,      // if "0" then everything, otherwise there must be a unique identifier
     * ];
     * ```
     *
     * @return string
     */
    private function getQueryString(array $params): string
    {
        $queryString = '';

        foreach ($params as $key => $value) {
            $queryString .= $key . '=' . $value . (next($params) ? '&' : '');
        }

        return $queryString;
    }

    /**
     * Sets the $catNumbersFromDb property
     */
    private function setCatNumbersFromDb()
    {
        $catNums = Coin::findAllCatalogueNumbers();

        foreach ($catNums as $catNum) {
            $this->catNumbersFromDb[] = $catNum['catalogue_number'];
        }
    }

    /**
     * @param array $catNumbers
     * @param string $urlBaseCoins
     */
    private function process(array $catNumbers, string $urlBaseCoins)
    {
        foreach ($catNumbers as $catNumber) {
            $url = $urlBaseCoins . 'ShowCoins/?cat_num=' . $catNumber;
            $cbrCoinService = new CbrCoinService($url);

            if (!$cbrCoinService->create()) {
                $num = $this->ansiFormat($catNumber, Console::FG_RED);
                echo 'Coin with catalog number: '
                    . $num . ' has not been processed' . "\n";
            }

            $num = $this->ansiFormat($catNumber, Console::FG_GREEN);
            echo 'Coin with catalog number: ' . $num . ' processed' . "\n";
        }

        echo $this->ansiFormat('Done!', Console::FG_CYAN) . "\n";
    }

    /**
     * Gets an array of the difference between the extracted data
     * and from the database
     */
    private function getArrayDiffExtractedAndFromDb(): array
    {
        return array_diff($this->catNumbers, $this->catNumbersFromDb);
    }
}
