<?php

namespace services;

use Yii;
use common\models\{
    Series, Coin, Details
};

/**
 * Class FlashHelper
 * @package services
 */
class FlashHelper
{
    public static function setFlashSaveImageError()
    {
        self::isCli() ?: Yii::$app->session->setFlash(
            'error',
            'Ошибка сохранения изображения. Скорее всего это связано с отсутствием прав на запись файла.'
        );
    }

    /**
     * @param Coin $coin
     */
    public static function setFlashSaveCoinErrors(Coin $coin)
    {
        self::isCli() ?: Yii::$app->session->setFlash('error', $coin->getFirstErrors());
    }

    /**
     * @param Coin $coin
     */
    public static function addFlashSaveCoinSuccess(Coin $coin)
    {
        self::isCli() ?: Yii::$app->session->addFlash(
            'success', 'Монета: ' . $coin->name . ' была успешно сохранена.'
        );
    }

    /**
     * @param Series $series
     */
    public static function addFlashSaveSeriesSuccess(Series $series)
    {
        self::isCli() ?: Yii::$app->session->addFlash(
            'success', 'Серия: ' . $series->name . ' была успешно сохранена.'
        );
    }

    /**
     * @param Series $series
     */
    public static function setFlashSaveSeriesErrorByName(Series $series)
    {
        self::isCli() ?: Yii::$app->session->setFlash('error', $series->getFirstError('name'));
    }

    public static function setFlashInvalidUrlError()
    {
        self::isCli() ?: Yii::$app->session->setFlash('error', 'Invalid URL!');
    }

    /**
     * @param Details $item
     * @param string $result
     */
    public static function addFlashSaveDetailsItemSuccessByResult(Details $item, string $result)
    {
        self::isCli() ?: Yii::$app->session->addFlash(
            'success',
            'Вариант исполнения монеты (ID: ' . $item->id . ') был успешно ' . $result . '.'
        );
    }

    /**
     * @param Details $item
     */
    public static function setFlashSaveDetailsItemErrors(Details $item)
    {
        self::isCli() ?: Yii::$app->session->setFlash('error', $item->getFirstErrors());
    }

    /**
     * @return bool
     */
    public static function isCli()
    {
        return !http_response_code();
    }
}
