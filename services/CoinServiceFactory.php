<?php

namespace services;

/**
 * Class CoinServiceFactory
 * @package services
 */
class CoinServiceFactory
{
    /**
     * @param $url
     * @return bool|object
     */
    public static function build($url)
    {
        // retrieves the class name from the resource host
        $parseUrl = parse_url($url);
        $className = __NAMESPACE__ . '\\' . ucfirst(substr($parseUrl['host'], 0, 3)) . 'CoinService';

        if (class_exists($className)) {
            if (($service = new $className($url)) && $service instanceof Parseable) {
                return $service;
            }
        }

        FlashHelper::setFlashInvalidUrlError();
        return false;
    }
}
