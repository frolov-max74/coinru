<?php

namespace services;

use Yii;
use common\models\{
    Series, Coin, Details
};

/**
 * Class CoinService
 * @package services
 */
abstract class CoinService implements Parseable
{
    protected $url;

    protected $name = '';
    protected $seriesName = '';
    protected $obverse = '';
    protected $reverse = '';
    protected $description = '';
    protected $catalogueNumber = '';
    protected $releaseDate = 0;
    protected $thematicInfo = '';
    protected $dignity = '';
    protected $material = '';
    protected $mint = [];
    protected $details = [];

    /**
     * CoinService constructor.
     * @param string $url url resource
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    abstract function setPropertiesFromHtml();

    /**
     * Creates a new coin or updates an existing one.
     * @return bool
     */
    public function create()
    {
        $this->setPropertiesFromHtml();

        $series = $this->getSeries($this->seriesName);
        $coin = $this->getCoin($this->catalogueNumber);

        $coin->series_id = $series ? $series->id : null;
        $coin->name = $this->name;
        if (!$coin->obverse = $this->saveImage($this->obverse, $this->catalogueNumber)) {
            FlashHelper::setFlashSaveImageError();
            $coin->obverse = '';
            return false;
        }
        if (!$coin->reverse = $this->saveImage($this->reverse, $this->catalogueNumber, 'r')) {
            FlashHelper::setFlashSaveImageError();
            $coin->reverse = '';
            return false;
        }
        $coin->description = $this->description;
        $coin->catalogue_number = $this->catalogueNumber;
        $coin->release_date = $this->releaseDate;
        $coin->thematic_info = $this->thematicInfo;
        $coin->dignity = $this->dignity;
        $coin->material = $this->material;
        $coin->mint = $this->mint;
        if (!$coin->save()) {
            FlashHelper::setFlashSaveCoinErrors($coin);
            return false;
        } else {
            FlashHelper::addFlashSaveCoinSuccess($coin);
        }

        if ($coin->details) {
            $this->updateDetails($coin->details, $this->details, $coin->id);
        } else {
            $this->createDetails($this->details, $coin->id);
        }

        return true;
    }

    /**
     * @param string $name
     * @return Series|null
     */
    private function getSeries(string $name)
    {
        if ('' !== $name) {
            if ($series = Series::findOne(['name' => $name])) {
                return $series;
            }
            $series = new Series();
            $series->name = $name;
        } else {
            if ($series = Series::findOne(['name' => '-'])) {
                return $series;
            }
            $series = new Series();
            $series->name = '-';
        }

        if (!$series->save()) {
            FlashHelper::setFlashSaveSeriesErrorByName($series);
            return null;
        } else {
            FlashHelper::addFlashSaveSeriesSuccess($series);
        }
        return $series;
    }

    /**
     * @param string $catalogueNumber
     * @return Coin
     */
    private function getCoin(string $catalogueNumber): Coin
    {
        if ($coin = Coin::findOne(['catalogue_number' => $catalogueNumber])) {
            return $coin;
        }
        return new Coin();
    }

    /**
     * @param string $source
     * @param string $catalogueNumber
     * @param string $reverse
     * @return bool|string
     */
    private function saveImage(string $source, string $catalogueNumber, string $reverse = '')
    {
        $basePath = Yii::getAlias('@frontend/web');
        $relPathImage = '/upload/images/coinru/' . $catalogueNumber . $reverse . '.jpg';
        $destPathImage = $basePath . $relPathImage;
        if (function_exists('curl_version')) {
            $ch = curl_init($source);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $image = curl_exec($ch);
            curl_close($ch);
        } else {
            $image = file_get_contents($source);
        }

        if (!@file_put_contents($destPathImage, $image, LOCK_EX)) {
            return false;
        }
        return $relPathImage;
    }

    /**
     * @param array $details
     * @param int $coinID
     * @return void
     */
    private function createDetails(array $details, int $coinID)
    {
        if ($details) {
            foreach ($details as $content) {
                $item = new Details();
                $item->coin_id = $coinID;
                $item->content = $content;
                $this->processingDetailsItemByAction($item, 'save');
            }
        }
    }

    /**
     * @param Details[] $coinDetails
     * @param array $details this details
     * @param  int $coinID
     * @return void
     */
    private function updateDetails(array $coinDetails, array $details, int $coinID)
    {
        if (count($coinDetails) >= count($details)) {
            foreach ($coinDetails as $key => $item) {
                if (isset($details[$key])) {
                    $item->content = $details[$key];
                    $this->processingDetailsItemByAction($item, 'save');
                } else {
                    $this->processingDetailsItemByAction($item, 'delete');
                }
            }
        } else {
            foreach ($details as $key => $item) {
                if (!isset($coinDetails[$key])) {
                    $detailsItem = new Details();
                    $detailsItem->coin_id = $coinID;
                    $detailsItem->content = $item;
                    $this->processingDetailsItemByAction($detailsItem, 'save');
                } else {
                    $coinDetails[$key]->content = $details[$key];
                    $this->processingDetailsItemByAction($coinDetails[$key], 'save');
                }
            }
        }
    }

    /**
     * @param Details $item
     * @param string $action
     * @return bool
     * @throws \Exception
     */
    private function processingDetailsItemByAction(Details $item, string $action)
    {
        switch ($action) {
            case 'save':
                $result = 'сохранен';
                break;
            case 'delete':
                $result = 'удален';
                break;
            default:
                throw new \Exception('Action must be \'save\' or \'delete\'');
        }
        if ($item->$action()) {
            FlashHelper::addFlashSaveDetailsItemSuccessByResult($item, $result);
            return true;
        }
        FlashHelper::setFlashSaveDetailsItemErrors($item);
        return false;
    }

}
