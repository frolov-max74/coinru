<?php

namespace services;

/**
 * Interface Parseable
 * @package services
 */
interface Parseable
{
    /**
     * @return mixed
     */
    public function setPropertiesFromHtml();
}
