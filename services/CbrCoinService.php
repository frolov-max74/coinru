<?php

namespace services;

use DiDom\{
    Document, Element
};

/**
 * Class CbrCoinService
 * @package services
 */
class CbrCoinService extends CoinService
{
    private $parseUrl = [];
    private $divAccentBlock = null;
    private $divSeven = null;

    /**
     * Sets coin data from html
     */
    public function setPropertiesFromHtml(): self
    {
        $document = new Document($this->url, true);
        $this->setParseUrl($this->url);
        $this->setNameFromDocument($document);
        $this->setSeriesNameFromDocument($document);
        $this->setObverseAndReverseFromDocument($document);
        $this->setDivSevenFromDocument($document);
        $this->setDescriptionFromElement($this->divSeven);
        $this->setMintFromElement($this->divSeven);
        $this->setCatalogueNumber();
        $this->setReleaseDateFromDocument($document);
        $this->setThematicInfoFromDocument($document);
        $this->setDivAccentBlockFromDocument($document);
        $this->setDignityFromElement($this->divAccentBlock);
        $this->setMaterialFromElement($this->divAccentBlock);
        $this->setDetailsFromElement($this->divAccentBlock);
        $this->resetParseUrl();
        $this->resetDivAccentBlock();
        $this->resetDivSeven();
        return $this;
    }

    /**
     * @param string $url
     */
    private function setParseUrl(string $url)
    {
        $this->parseUrl = parse_url($url);
    }

    /**
     * @param Document $document
     */
    private function setNameFromDocument(Document $document)
    {
        if ($name = $document->first('span.referenceable::text')) {
            $this->name = trim($name);
        }
    }

    /**
     * @param Document $document
     */
    private function setSeriesNameFromDocument(Document $document)
    {
        if ($seriesName = $this->getSeriesNameFromDocument($document)) {
            $this->seriesName = $this->filterSeriesName($seriesName);
        }
    }

    /**
     * @param Document $document
     * @return bool|string
     */
    private function getSeriesNameFromDocument(Document $document)
    {
        if ($seriesName = $document->first('#content > p > em::text')) {
            return trim($seriesName);
        }
        return false;
    }

    /**
     * @param string $seriesName
     * @return string
     */
    private function filterSeriesName(string $seriesName)
    {
        $search = [
            'Серия:',
            'Cерия:', // "C" latin
            'Серия',
            'Историческая серия:',
            'Географическая серия:',
            'Набор памятных монет:',
            'Спортивная серия:',
        ];
        $arrayFromSeriesName = explode(
            ' ',
            trim(str_replace($search, '', $seriesName))
        );
        foreach ($arrayFromSeriesName as $key => $item) {
            if (strlen($item) == 0) {
                // remove extra spaces between words in the series name
                unset($arrayFromSeriesName[$key]);
            }
        }
        return implode(' ', $arrayFromSeriesName);
    }

    /**
     * @param Document $document
     */
    private function setObverseAndReverseFromDocument(Document $document)
    {
        $baseUrl = $this->getBaseUrl();
        $images = $this->getImagesFromDocument($document);
        if (!empty($images)) {
            /**
             * @var $image Element
             */
            foreach ($images as $key => $image) {
                $key === 0 ? $this->obverse = $baseUrl . $image->getAttribute('src') :
                    $this->reverse = $baseUrl . $image->getAttribute('src');
            }
        }
    }

    /**
     * @return string
     */
    private function getBaseUrl()
    {
        return $this->parseUrl['scheme'] . '://' . $this->parseUrl['host'];
    }

    /**
     * @param Document $document
     * @return Element[]|\DOMElement[]
     */
    private function getImagesFromDocument(Document $document)
    {
        return $document->find('div.five.gcolumns > a > img');
    }

    /**
     * @param Document $document
     */
    private function setDivSevenFromDocument(Document $document)
    {
        if ($divSeven = $this->getDivSevenFromDocument($document)) {
            $this->divSeven = $divSeven;
        }
    }

    /**
     * @param Document $document
     * @return bool|Element|\DOMElement|null
     */
    private function getDivSevenFromDocument(Document $document)
    {
        return $document->first('div.seven');
    }

    /**
     * @param Element $element
     */
    private function setDescriptionFromElement(Element $element)
    {
        if ($description = $this->getDescriptionFromElement($element)) {
            $this->description = $description;
        }
    }

    /**
     * @param Element $element
     * @return bool|string
     */
    private function getDescriptionFromElement(Element $element)
    {
        if ($children = $element->children()) {
            $description = '';
            foreach ($children as $child) {
                if ($child->isTextNode() || $child->has('div')) {
                    continue;
                }
                $description .= $child->html();
            }
            return $description;
        }
        return false;
    }

    /**
     * @param Element $element
     */
    private function setMintFromElement(Element $element)
    {
        if ($mint = $this->getMintFromElement($element)) {
            $this->mint = $mint;
        }
    }

    /**
     * @param Element $element
     * @return array|bool
     */
    private function getMintFromElement(Element $element)
    {
        if ($h2s = $element->find('h2')) {
            $mint = [];
            foreach ($h2s as $h2) {
                if ('Авторы' !== $h2->text()) {
                    continue;
                }
                // search in the first paragraph after 'h2'
                $mint = $this->getArrayOfMints($h2->nextSibling('p')->text());
                if (empty($mint)) {
                    // search in the second paragraph after 'h2'
                    $mint = $this->getArrayOfMints(
                        $h2->nextSibling('p')->nextSibling('p')->text()
                    );
                }
            }
            return $mint;
        }
        return false;
    }

    /**
     * @param string $string
     * @return array
     */
    private function getArrayOfMints(string $string)
    {
        $mint = [];
        if (false !== strpos($string, 'ЛМД') ||
            false !== strpos($string, 'СМД') // "СМД" cbr.ru error
        ) {
            $mint[] = 'ЛМД';
        } elseif (false !== strpos($string, 'СПМД') ||
            false !== strpos($string, 'Санкт-Петербургский монетный двор')
        ) {
            $mint[] = 'СПМД';
        }
        if (false !== strpos($string, 'ММД') ||
            false !== strpos($string, 'Московский монетный двор')
        ) {
            $mint[] = 'ММД';
        }
        return $mint;
    }

    /**
     * Sets catalogue number from query string
     */
    private function setCatalogueNumber()
    {
        if ($queryString = $this->getQueryString()) {
            if ($catalogueNumber = $this->filterCatalogueNumber($queryString)) {
                $this->catalogueNumber = $catalogueNumber;
            }
        }
    }

    /**
     * @return bool|mixed
     */
    private function getQueryString()
    {
        return $this->parseUrl['query'] ?? false;
    }

    /**
     * @param string $queryString
     * @return bool|string
     */
    private function filterCatalogueNumber(string $queryString)
    {
        if (false !== strpos($queryString, 'cat_num=')) {
            return trim(str_replace('cat_num=', '', $queryString));
        }
        return false;
    }

    private function setReleaseDateFromDocument(Document $document)
    {
        if ($releaseDateString = $this->getReleaseDateStringFromDocument($document)) {
            if ($releaseDate = $this->filterReleaseDate($releaseDateString)) {
                $this->releaseDate = $releaseDate;
            }
        }
    }

    /**
     * @param Document $document
     * @return Element|\DOMElement|null
     */
    private function getReleaseDateStringFromDocument(Document $document)
    {
        return $document->first('div.accent_block > p > i::text');
    }

    /**
     * @param string $releaseDateString
     * @return bool|false|int
     */
    private function filterReleaseDate(string $releaseDateString)
    {
        if (false !== strpos($releaseDateString, 'Дата выпуска:')) {
            return strtotime(trim(str_replace('Дата выпуска:', '', $releaseDateString))) ?? false;
        }
        return false;
    }

    /**
     * @param Document $document
     */
    private function setThematicInfoFromDocument(Document $document)
    {
        if ($divAnswer = $this->getDivAnswerFromDocument($document)) {
            foreach ($divAnswer->children() as $element) {
                if ($element->isTextNode()) {
                    continue;
                }
                $this->appendThematicInfoFromElement($element);
            }
        }
    }

    /**
     * @param Document $document
     * @return Element|\DOMElement|null
     */
    private function getDivAnswerFromDocument(Document $document)
    {
        return $document->first('div.answer');
    }

    /**
     * @param Element $element
     */
    private function appendThematicInfoFromElement(Element $element)
    {
        $this->thematicInfo .= $element->html();
    }

    /**
     * @param Document $document
     */
    private function setDivAccentBlockFromDocument(Document $document)
    {
        if ($divAccentBlock = $this->getDivAccentBlockFromDocument($document)) {
            $this->divAccentBlock = $divAccentBlock;
        }
    }

    /**
     * @param Document $document
     * @return Element|\DOMElement|null
     */
    private function getDivAccentBlockFromDocument(Document $document)
    {
        return $document->first('div.accent_block');
    }

    /**
     * @param Element $element
     */
    private function setDignityFromElement(Element $element)
    {
        if ($dignity = $this->getDignityFromElement($element)) {
            $this->dignity = $dignity;
        }
    }

    /**
     * @param Element $element
     * @return bool|string
     */
    private function getDignityFromElement(Element $element)
    {
        if ($tr = $element->first('table > tr')) {
            return $tr->first('td')->nextSibling('td')->text() ?? false;
        }
        return false;
    }

    /**
     * @param Element $element
     */
    private function setMaterialFromElement(Element $element)
    {
        if ($material = $this->getMaterialFromElement($element)) {
            $this->material = $material;
        }
    }

    /**
     * @param Element $element
     * @return bool|string
     */
    private function getMaterialFromElement(Element $element)
    {
        if ($trs = $element->find('table > tr')) {
            /**
             * @var Element $tr
             */
            foreach ($trs as $key => $tr) {
                if (2 !== $key) {
                    continue;
                }
                return trim($tr->first('td')->nextSibling('td')->text()) ?? false;
            }
        }
        return false;
    }

    /**
     * @param Element $element
     */
    private function setDetailsFromElement(Element $element)
    {
        if ($details = $this->getDetailsFromElement($element)) {
            $this->details = $details;
        }
    }

    /**
     * @param Element $element
     * @return array|bool
     */
    private function getDetailsFromElement(Element $element)
    {
        if ($tables = $element->find('table')) {
            $details = [];
            /**
             * @var Element $table
             */
            foreach ($tables as $table) {
                $details[] = $table->html();
            }
            return $details;
        }
        return false;
    }

    /**
     * Reset this parseUrl
     */
    private function resetParseUrl()
    {
        $this->parseUrl = [];
    }

    /**
     * Reset this divAccentBlock
     */
    private function resetDivAccentBlock()
    {
        $this->divAccentBlock = null;
    }

    /**
     * Reset this divSeven
     */
    private function resetDivSeven()
    {
        $this->divSeven = null;
    }

}
