<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%details}}".
 *
 * @property int $id
 * @property int $coin_id
 * @property string $content
 *
 * @property Coin $coin
 */
class Details extends yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%details}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coin_id'], 'required'],
            [['coin_id'], 'integer'],
            [['content'], 'string'],
            [['coin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coin::class, 'targetAttribute' => ['coin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coin_id' => 'Монета',
            'content' => 'Параметры',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoin()
    {
        return $this->hasOne(Coin::class, ['id' => 'coin_id']);
    }
}
