<?php

namespace common\models\chart;

use common\models\db\Coin as DBCoin;
use common\models\Coin;

/**
 * Class CoinsMap
 * @package common\models\chart
 */
class CoinsMap
{
    public $coinDates = [];

    /**
     * Builds an array of data
     *
     * @return array
     */
    public function getFull()
    {
        $this->coinDates = DBCoin::findAllReleaseDates();
        $AllDatesAndQuantities = $this->getAllDatesAndQuantities();

        $result = [
            'allDates' => $AllDatesAndQuantities[0],
            'allQuantities' => $AllDatesAndQuantities[1],
            'quantitiesByAu' => $this->getQuantitiesByParams(
                'золото',
                'findReleaseDatesByPrecious'
            ),
            'quantitiesByAg' => $this->getQuantitiesByParams(
                'серебро',
                'findReleaseDatesByPrecious'
            ),
            'quantitiesByPt' => $this->getQuantitiesByParams(
                'платина',
                'findReleaseDatesByPrecious'
            ),
            'quantitiesByPd' => $this->getQuantitiesByParams(
                'палладий',
                'findReleaseDatesByPrecious'
            ),
            'quantitiesByPrecious' => $this->getQuantitiesByParams(
                true,
                'findReleaseDatesByPrecious'
            ),
            'quantitiesByNotPrecious' => $this->getQuantitiesByParams(
                false,
                'findReleaseDatesByPrecious'
            ),
            'quantitiesByRFSeries' => $this->getQuantitiesByParams(
                'Российская Федерация',
                'findReleaseDatesBySeries'
            ),
            'quantitiesWithoutSeries' => $this->getQuantitiesByParams(
                '-',
                'findReleaseDatesBySeries'
            ),
            'quantitiesByRedBookSeries' => $this->getQuantitiesByParams(
                'Красная книга',
                'findReleaseDatesBySeries'
            ),
            'quantitiesByAncientTownsSeries' => $this->getQuantitiesByParams(
                'Древние города России',
                'findReleaseDatesBySeries'
            ),
            'quantitiesByProminentFiguresSeries' => $this->getQuantitiesByParams(
                'Выдающиеся личности России',
                'findReleaseDatesBySeries'
            ),
            'quantitiesByMilitaryGloryCitiesSeries' => $this->getQuantitiesByParams(
                'Города воинской славы',
                'findReleaseDatesBySeries'
            ),
            'quantitiesByCommemorative' => $this->getQuantitiesByParams(
                Coin::TYPE_COMMEMORATIVE,
                'findReleaseDatesByType'
            ),
            'quantitiesByInvestment' => $this->getQuantitiesByParams(
                Coin::TYPE_INVESTMENT,
                'findReleaseDatesByType'
            ),
            'quantitiesByLMD' => $this->getQuantitiesByParams(
                Coin::MINT_LMD,
                'findReleaseDatesByMint'
            ),
            'quantitiesBySPMD' => $this->getQuantitiesByParams(
                Coin::MINT_SPMD,
                'findReleaseDatesByMint'
            ),
            'quantitiesByMMD' => $this->getQuantitiesByParams(
                Coin::MINT_MMD,
                'findReleaseDatesByMint'
            ),
        ];

        return $result;
    }

    /**
     * Gets all dates and quantities
     *
     * @return array
     */
    protected function getAllDatesAndQuantities()
    {
        $dates = [];
        $quantities = [];

        foreach ($this->coinDates as $coinDate) {
            $year = date('Y', $coinDate['release_date']);
            $dates[] = $year;
            isset($quantities[$year]) ? $quantities[$year] += 1 : $quantities[$year] = 1;
        }
        $uniqueDates = array_unique($dates);
        asort($uniqueDates);
        ksort($quantities);

        return [array_values($uniqueDates), array_values($quantities)];
    }

    /**
     * Receives quantities depending on the parameters transferred
     *
     * @param bool|string|int $name The name of the series or the name of the precious metal or logical value,
     * the type of coin or the name of the mint.
     * @param string $methodName If $name is passed true or false,
     * then the method name must be 'findReleaseDatesByPrecious'. If $name is passed string,
     * then the method name must be 'findReleaseDatesByPrecious' or 'findReleaseDatesBySeries' or
     * 'findReleaseDatesByMint'. If $name is passed integer, then the method name must be 'findReleaseDatesByType'.
     * @return array
     */
    protected function getQuantitiesByParams($name, string $methodName)
    {
        $quantities = [];

        foreach ($this->coinDates as $coinDate) {
            $year = date('Y', $coinDate['release_date']);
            $quantities[$year] = 0;
        }

        $coinDates = DBCoin::$methodName($name);

        foreach ($coinDates as $coinDate) {
            $year = date('Y', $coinDate['release_date']);
            $quantities[$year] += 1;
        }
        ksort($quantities);

        return array_values($quantities);
    }
}
