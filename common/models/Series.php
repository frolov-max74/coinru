<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%series}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Coin[] $coins
 */
class Series extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%series}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoins()
    {
        return $this->hasMany(Coin::class, ['series_id' => 'id']);
    }

    /**
     * @return array array of the names of the series
     */
    public static function getAll()
    {
        return self::find()->select(['name', 'id'])->indexBy('id')->column();
    }
}
