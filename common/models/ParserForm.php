<?php

namespace common\models;

use yii\base\Model;

/**
 * Class ParserForm
 * @package common\models
 */
class ParserForm extends Model
{
    public $url;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            // url are required
            [['url',], 'required'],
        ];
    }
}
