<?php

namespace common\models\db;

use Yii;
use yii\base\Model;

/**
 * Class Coin
 * @package common\models\db
 */
class Coin extends Model
{
    /**
     * @return array
     */
    public static function findAllReleaseDates()
    {
        return Yii::$app
            ->db
            ->createCommand(
                'SELECT ' .
                'release_date ' .
                'FROM ' .
                'coin'
            )
            ->queryAll();
    }

    /**
     * @param string $name
     * @return array
     */
    public static function findReleaseDatesBySeries(string $name)
    {
        return Yii::$app
            ->db
            ->createCommand(
                'SELECT ' .
                'release_date ' .
                'FROM coin as c ' .
                'LEFT JOIN series as s ON c.series_id = s.id ' .
                'WHERE s.name = :series'
            )
            ->bindValue(':series', $name)
            ->queryAll();
    }

    /**
     * @param string|bool $precious
     * @return array|bool
     */
    public static function findReleaseDatesByPrecious($precious)
    {
        switch (gettype($precious)) {
            case 'boolean':
                $not = $precious ? '' : 'NOT';
                return Yii::$app
                    ->db
                    ->createCommand(
                        'SELECT ' .
                        'release_date ' .
                        'FROM coin ' .
                        'WHERE material ' . $not . ' REGEXP \'[0-9]\''
                    )
                    ->queryAll();
            case 'string':
                return Yii::$app
                    ->db
                    ->createCommand(
                        'SELECT ' .
                        'release_date ' .
                        'FROM coin ' .
                        'WHERE material LIKE :precious'
                    )
                    ->bindValue(':precious', '%' . $precious . '%')
                    ->queryAll();
            default:
                return false;
        }
    }

    /**
     * @param int $type
     * @return array
     */
    public function findReleaseDatesByType(int $type)
    {
        return Yii::$app
            ->db
            ->createCommand(
                'SELECT ' .
                'release_date ' .
                'FROM coin ' .
                'WHERE type = :type'
            )
            ->bindValue(':type', $type)
            ->queryAll();
    }

    /**
     * @param string $mint
     * @return array
     */
    public function findReleaseDatesByMint(string $mint)
    {
        return Yii::$app
            ->db
            ->createCommand(
                'SELECT ' .
                'release_date ' .
                'FROM coin ' .
                'WHERE mint LIKE :mint'
            )
            ->bindValue(':mint', '%' . $mint . '%')
            ->queryAll();
    }

    /**
     * @return array
     */
    public static function findAllCatalogueNumbers()
    {
        return Yii::$app
            ->db
            ->createCommand(
                'SELECT ' .
                'catalogue_number ' .
                'FROM ' .
                'coin'
            )
            ->queryAll();
    }
}
