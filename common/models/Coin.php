<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%coin}}".
 *
 * @property int $id
 * @property string $name
 * @property string $obverse
 * @property string $reverse
 * @property string $description
 * @property string $catalogue_number
 * @property int $release_date
 * @property string $thematic_info
 * @property string $dignity
 * @property string $material
 * @property string $mint
 * @property integer $type
 * @property int $series_id
 *
 * @property Series $series
 * @property Details[] $details
 */
class Coin extends ActiveRecord
{
    const TYPE_COMMEMORATIVE = 0;
    const TYPE_INVESTMENT = 1;
    const MINT_LMD = 'ЛМД';
    const MINT_SPMD = 'СПМД';
    const MINT_MMD = 'ММД';
    public $obverseFile;
    public $reverseFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%coin}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'obverse',
                    'reverse',
                    'description',
                    'catalogue_number',
                    'release_date',
                    'dignity',
                    'material',
                    'mint'
                ],
                'required'
            ],
            [
                [
                    'obverseFile',
                    'reverseFile',
                ],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => 'png, jpg',
                'checkExtensionByMimeType' => false
            ],
            [['description', 'thematic_info'], 'string'],
            [['series_id'], 'integer'],
            [
                [
                    'release_date',
                ],
                'date',
                'format' => 'dd.MM.yyyy',
                'timestampAttribute' => 'release_date'
            ],
            [['obverse', 'reverse'], 'string', 'max' => 150],
            [['name'], 'string', 'max' => 200],
            [['catalogue_number', 'dignity'], 'string', 'max' => 20],
            [['catalogue_number'], 'unique'],
            [['mint'], 'filter', 'filter' => function ($value) {
                return json_encode($value, JSON_UNESCAPED_UNICODE);
            }],
            [['material', 'mint'], 'string', 'max' => 100],
            ['type', 'default', 'value' => self::TYPE_COMMEMORATIVE],
            [
                'type',
                'in',
                'range' => [
                    self::TYPE_COMMEMORATIVE,
                    self::TYPE_INVESTMENT
                ]
            ],
            [
                ['series_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Series::class,
                'targetAttribute' => ['series_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'obverse' => 'Аверс',
            'reverse' => 'Реверс',
            'description' => 'Описание',
            'catalogue_number' => 'Номер',
            'release_date' => 'Дата выпуска',
            'thematic_info' => 'Историко-тематическая справка',
            'dignity' => 'Номинал',
            'material' => 'Материал',
            'mint' => 'Монетный двор',
            'type' => 'Тип',
            'series_id' => 'Серия',
        ];
    }

    /**
     * Upload the image files
     * @param string $image 'obverse' or 'reverse'
     */
    public function uploadImage($image = 'obverse')
    {
        $basePath = Yii::getAlias('@frontend/web');
        $imageFile = $image . 'File';

        if ($this->$imageFile = UploadedFile::getInstance($this, $imageFile)) {
            $this->deleteImageFile($this->$image);
            $relImagePath = '/upload/images/coinru/' . time() . '_' .
                $this->$imageFile->baseName . '.' . $this->$imageFile->extension;
            $destPath = $basePath . $relImagePath;
            if ($this->$imageFile->saveAs($destPath)) {
                $this->$image = $relImagePath;
            }
        }
    }

    /**
     * After finding the mint is decoded
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->mint = json_decode($this->mint);
    }

    /**
     * Before deleting a record deletes the image files
     * @return bool
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        $this->deleteImageFile($this->obverse);
        $this->deleteImageFile($this->reverse);
        return true;
    }

    /**
     * Deletes the image file
     * @var string $image path to image file
     */
    protected function deleteImageFile(?string $image)
    {
        if (is_file(Yii::getAlias('@frontend/web' . $image))) {
            unlink(Yii::getAlias('@frontend/web' . $image));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::class, ['id' => 'series_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetails()
    {
        return $this->hasMany(Details::class, ['coin_id' => 'id']);
    }
}
