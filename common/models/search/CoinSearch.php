<?php

namespace common\models\search;

use kartik\daterange\DateRangeBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Coin;

/**
 * CoinSearch represents the model behind the search form of `common\models\Coin`.
 */
class CoinSearch extends Coin
{
    public $date_from;
    public $date_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'series_id'], 'integer'],
            [['release_date'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [
                [
                    'name',
                    'description',
                    'catalogue_number',
                    'thematic_info',
                    'dignity',
                    'material',
                    'mint',
                ],
                'safe'
            ],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::class,
                'attribute' => 'release_date',
                'dateStartAttribute' => 'date_from',
                'dateEndAttribute' => 'date_to',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coin::find()->with('series');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'series_id' => $this->series_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'catalogue_number', $this->catalogue_number])
            ->andFilterWhere(['like', 'dignity', $this->dignity])
            ->andFilterWhere(['like', 'material', $this->material])
            ->andFilterWhere(['>=', 'release_date', $this->date_from])
            ->andFilterWhere(['<=', 'release_date', $this->date_to])
            ->andFilterWhere(['like', 'mint', $this->mint]);

        return $dataProvider;
    }
}
