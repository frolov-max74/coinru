<?php

namespace common\helpers;

use common\models\Coin;
use yii\helpers\{
    ArrayHelper, Html
};

/**
 * Class CoinHelper
 * @package common\helpers
 */
class CoinHelper
{
    /**
     * Returns a list of coin types.
     * @return array
     */
    public static function typeList(): array
    {
        return [
            Coin::TYPE_COMMEMORATIVE => 'ПАМЯТНАЯ',
            Coin::TYPE_INVESTMENT => 'ИНВЕСТИЦИОННАЯ',
        ];
    }

    /**
     * Returns the name of the type depending on the type value passed.
     * @param $type
     * @return string
     */
    protected static function typeName($type): string
    {
        return ArrayHelper::getValue(self::typeList(), $type);
    }

    /**
     * Returns type label depending on the type passed.
     * @param $type
     * @return string
     */
    public static function typeLabel($type): string
    {
        switch ($type) {
            case Coin::TYPE_COMMEMORATIVE:
                $class = 'label label-info';
                break;
            case Coin::TYPE_INVESTMENT:
                $class = 'label label-warning';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', self::typeName($type), [
            'class' => $class,
        ]);
    }

    /**
     * Returns a list of coin mints.
     * @return array
     */
    public static function mintList(): array
    {
        return [
            Coin::MINT_LMD => 'ЛМД',
            Coin::MINT_MMD => 'ММД',
            Coin::MINT_SPMD => 'СПМД',
        ];
    }

    /**
     * Returns mint labels.
     * @param array $mints
     * @return string
     */
    public static function mintLabel($mints): string
    {
        $mintsArray = [];
        foreach ($mints as $mint) {
            switch ($mint) {
                case Coin::MINT_LMD:
                    $class = 'label label-success';
                    break;
                case Coin::MINT_MMD:
                    $class = 'label label-danger';
                    break;
                case Coin::MINT_SPMD:
                    $class = 'label label-success';
                    break;
                default:
                    $class = 'label label-default';
            }
            $mintsArray[] = Html::tag('span', $mint, [
                'class' => $class,
            ]);
        }
        return implode(' ', $mintsArray);
    }
}
