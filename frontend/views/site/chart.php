<?php

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data array */

$this->title = 'Графики выпуска памятных и инвестиционных монет России';
?>
<div class="text-center">
    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
</div>
<div class="row">
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Все памятные и инвестиционные монеты'],
                'subtitle' => ['text' => '(Количество всех монет, выпущенных в разные годы)'],
                'chart' => [
                    'type' => 'spline',
                ],

                'xAxis' => [
                    'type' => 'date',
                    'categories' => $data['allDates'],
                ],
                'yAxis' => [
                    'title' => ['text' => 'Количество, шт.'],
                ],
                'plotOptions' => [
                    'spline' => [
                        'cursor' => 'pointer',
                    ],
                ],
                'series' => [
                    [
                        'name' => 'Монеты России',
                        'data' => $data['allQuantities'],
                        'marker' => [
                            'lineWidth' => 2,
                            'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                            'fillColor' => 'white',
                        ],
                    ],
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Драгоценные и недрагоценные монеты'],
                'subtitle' => ['text' => '(Количество выпущенных монет за все время и в разные годы)'],
                'chart' => [
                    'type' => 'column',
                ],

                'xAxis' => [
                    'type' => 'date',
                    'categories' => $data['allDates'],
                ],
                'yAxis' => [
                    'title' => ['text' => 'Количество, шт.'],
                ],
                'plotOptions' => [
                    'column' => [
                        'stacking' => 'normal',
                        'cursor' => 'pointer',
                    ],
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                    ],
                ],
                'series' => [
                    [
                        'name' => 'Драгоценные монеты',
                        'data' => $data['quantitiesByPrecious'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[4]'),
                    ],
                    [
                        'name' => 'Недрагоценные монеты',
                        'data' => $data['quantitiesByNotPrecious'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
                    ],
                    [
                        'type' => 'pie',
                        'name' => 'Всего выпущено',
                        'data' => [
                            [
                                'name' => 'Драгоценные монеты',
                                'y' => array_sum($data['quantitiesByPrecious']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[4]'),
                            ],
                            [
                                'name' => 'Недрагоценные монеты',
                                'y' => array_sum($data['quantitiesByNotPrecious']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
                            ],
                        ],
                        'center' => [100, 50],
                        'size' => 100,
                        'showInLegend' => false,
                        'dataLabels' => [
                            'enabled' => false,
                        ],
                    ],
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Драгоценные монеты из разных металлов'],
                'subtitle' => ['text' => '(Количество монет, выпущенных в разные годы)'],
                'chart' => [
                    'type' => 'column',
                ],

                'xAxis' => [
                    'type' => 'date',
                    'categories' => $data['allDates'],
                ],
                'yAxis' => [
                    'title' => ['text' => 'Количество, шт.'],
                ],
                'plotOptions' => [
                    'column' => [
                        'stacking' => 'normal',
                        'cursor' => 'pointer',
                    ],
                ],
                'series' => [
                    [
                        'name' => 'Платиновые монеты',
                        'data' => $data['quantitiesByPt'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[4]'),
                    ],
                    [
                        'name' => 'Палладиевые монеты',
                        'data' => $data['quantitiesByPd'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
                    ],
                    [
                        'name' => 'Золотые монеты',
                        'data' => $data['quantitiesByAu'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'),
                    ],
                    [
                        'name' => 'Серебряные монеты',
                        'data' => $data['quantitiesByAg'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
                    ],
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Драгоценные монеты из разных металлов'],
                'subtitle' => ['text' => '(Количество монет, выпущенных за все время)'],
                'chart' => [
                    'type' => 'pie',
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => ['enabled' => true],
                        'showInLegend' => true,
                    ],
                ],
                'series' => [
                    [ // new opening bracket
                        'type' => 'pie',
                        'name' => 'Всего выпущено',
                        'colorByPoint' => true,
                        'data' => [
                            [
                                'name' => 'Платиновые монеты',
                                'y' => array_sum($data['quantitiesByPt']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[4]'),
                            ],
                            [
                                'name' => 'Палладиевые монеты',
                                'y' => array_sum($data['quantitiesByPd']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
                            ],
                            [
                                'name' => 'Золотые монеты',
                                'y' => array_sum($data['quantitiesByAu']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[1]'),
                            ],
                            [
                                'name' => 'Серебряные монеты',
                                'y' => array_sum($data['quantitiesByAg']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
                            ],
                        ],
                    ], // new closing bracket
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Некоторые серии монет'],
                'subtitle' => ['text' => '(Количество монет, выпущенных за все время)'],
                'chart' => [
                    'type' => 'pie',
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => ['enabled' => false],
                        'showInLegend' => true,
                    ],
                ],
                'series' => [
                    [ // new opening bracket
                        'type' => 'pie',
                        'name' => 'Всего выпущено',
                        'colorByPoint' => true,
                        'data' => [
                            [
                                'name' => 'Без серии',
                                'y' => array_sum($data['quantitiesWithoutSeries']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[2]'),
                            ],
                            [
                                'name' => 'Выдающиеся личности России',
                                'y' => array_sum($data['quantitiesByProminentFiguresSeries']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[10]'),
                            ],
                            [
                                'name' => 'Российская Федерация',
                                'y' => array_sum($data['quantitiesByRFSeries']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
                            ],
                            [
                                'name' => 'Города воинской славы',
                                'y' => array_sum($data['quantitiesByMilitaryGloryCitiesSeries']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
                            ],
                            [
                                'name' => 'Красная книга',
                                'y' => array_sum($data['quantitiesByRedBookSeries']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[5]'),
                            ],
                            [
                                'name' => 'Древние города России',
                                'y' => array_sum($data['quantitiesByAncientTownsSeries']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[8]'),
                            ],
                        ],
                    ], // new closing bracket
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Некоторые серии монет'],
                'subtitle' => ['text' => '(Количество монет, выпущенных в разные годы)'],
                'chart' => [
                    'type' => 'column',
                ],

                'xAxis' => [
                    'type' => 'date',
                    'categories' => $data['allDates'],
                ],
                'yAxis' => [
                    'title' => ['text' => 'Количество, шт.'],
                ],
                'plotOptions' => [
                    'column' => [
                        'stacking' => 'normal',
                        'cursor' => 'pointer',
                    ],
                ],
                'series' => [
                    [
                        'name' => 'Без серии',
                        'data' => $data['quantitiesWithoutSeries'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'),
                    ],
                    [
                        'name' => 'Выдающиеся личности России',
                        'data' => $data['quantitiesByProminentFiguresSeries'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[10]'),
                    ],
                    [
                        'name' => 'Российская Федерация',
                        'data' => $data['quantitiesByRFSeries'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
                    ],
                    [
                        'name' => 'Города воинской славы',
                        'data' => $data['quantitiesByMilitaryGloryCitiesSeries'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
                    ],
                    [
                        'name' => 'Красная книга',
                        'data' => $data['quantitiesByRedBookSeries'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[5]'),
                    ],
                    [
                        'name' => 'Древние города России',
                        'data' => $data['quantitiesByAncientTownsSeries'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[8]'),
                    ],
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Памятные и инвестиционные монеты'],
                'subtitle' => ['text' => '(Количество монет, выпущенных в разные годы)'],
                'chart' => [
                    'type' => 'column',
                ],

                'xAxis' => [
                    'type' => 'date',
                    'categories' => $data['allDates'],
                ],
                'yAxis' => [
                    'title' => ['text' => 'Количество, шт.'],
                ],
                'plotOptions' => [
                    'column' => [
                        'stacking' => 'normal',
                        'cursor' => 'pointer',
                    ],
                ],
                'series' => [
                    [
                        'name' => 'Инвестиционные монеты',
                        'data' => $data['quantitiesByInvestment'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'),
                    ],
                    [
                        'name' => 'Памятные монеты',
                        'data' => $data['quantitiesByCommemorative'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
                    ],
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'Памятные и инвестиционные монеты'],
                'subtitle' => ['text' => '(Количество монет, выпущенных за все время)'],
                'chart' => [
                    'type' => 'pie',
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => ['enabled' => true],
                        'showInLegend' => true,
                    ],
                ],
                'series' => [
                    [ // new opening bracket
                        'type' => 'pie',
                        'name' => 'Всего выпущено',
                        'colorByPoint' => true,
                        'data' => [
                            [
                                'name' => 'Инвестиционные монеты',
                                'y' => array_sum($data['quantitiesByInvestment']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[2]'),
                            ],
                            [
                                'name' => 'Памятные монеты',
                                'y' => array_sum($data['quantitiesByCommemorative']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
                            ],
                        ],
                    ], // new closing bracket
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'ММД, СПМД (ЛМД)'],
                'subtitle' => ['text' => '(Количество монет, выпущенных монетными дворами за все время)'],
                'chart' => [
                    'type' => 'pie',
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => ['enabled' => false],
                        'showInLegend' => true,
                    ],
                ],
                'series' => [
                    [ // new opening bracket
                        'type' => 'pie',
                        'name' => 'Всего выпущено',
                        'colorByPoint' => true,
                        'data' => [
                            [
                                'name' => 'ЛМД',
                                'y' => array_sum($data['quantitiesByLMD']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[4]'),
                            ],
                            [
                                'name' => 'СПМД',
                                'y' => array_sum($data['quantitiesBySPMD']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[2]'),
                            ],
                            [
                                'name' => 'ММД',
                                'y' => array_sum($data['quantitiesByMMD']),
                                'color' => new JsExpression('Highcharts.getOptions().colors[5]'),
                            ],
                        ],
                    ], // new closing bracket
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
    <div class="chart-index col-md-6">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'setupOptions' => [
                'lang' => [
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'printChart' => 'Напечатать график'
                ],
            ],
            'options' => [
                'title' => ['text' => 'ММД, СПМД (ЛМД)'],
                'subtitle' => ['text' => '(Количество монет, выпущенных монетными дворами в разные годы)'],
                'chart' => [
                    'type' => 'column',
                ],

                'xAxis' => [
                    'type' => 'date',
                    'categories' => $data['allDates'],
                ],
                'yAxis' => [
                    'title' => ['text' => 'Количество, шт.'],
                ],
                'plotOptions' => [
                    'column' => [
                        'stacking' => 'normal',
                        'cursor' => 'pointer',
                    ],
                ],
                'series' => [
                    [
                        'name' => 'ЛМД',
                        'data' => $data['quantitiesByLMD'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[4]'),
                    ],
                    [
                        'name' => 'СПМД',
                        'data' => $data['quantitiesBySPMD'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'),
                    ],
                    [
                        'name' => 'ММД',
                        'data' => $data['quantitiesByMMD'],
                        'color' => new JsExpression('Highcharts.getOptions().colors[5]'),
                    ],
                ],
                'credits' => ['enabled' => false],
            ]
        ]); ?>
    </div>
</div>
